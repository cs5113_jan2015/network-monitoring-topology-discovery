# README #

### CDP-Topology ###
* Version 0.1

Finds network topology using snmp with CDP ( Cisco Discovery Protocol ) . 
 

### CDP-Topology ###

* Input : Only One IP Address of CDP enabled switch/router 
* Works for both SNMP V 2c and SNMP V3 
* Integrate with Neo4j database 
* Can be visualize with neo4j .
* NoSNMP file is used for maintaining the list of neighbors hosts which are not SNMP (v2 and v3 ) enabled. This will speed up the process.


### Running Environment ###

Ubuntu 14.04 LTS Server
Perl 5

### Install ###

You need some perl's SNMP module to run the program
```bash
cpan install Net::SNMP
cpan install REST::Neo4p
```

### Database configuration ###
* install neo4j if needed
* otherwise you can see results on console 

### Run Program ###
```bash
perl main.pl
```
### TODO ###
*Seperate config file for all database and SNMP configuration


### Contact ###

* [Om Prakash](mailto: cs14mtech11011@iith.ac.in)
* [Navroz Firoz](mailto: cs14mtech10003@iith.ac.in)
* [Uttam Dhabas](mailto: cs14mtech10018@iith.ac.in)
